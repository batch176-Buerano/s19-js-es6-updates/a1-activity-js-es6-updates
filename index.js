// console.log(`YOw`)

const getCube = 2 ** 1
console.log(`The cube of ${getCube} is 8`)


const address = [`Tarlac`, `Moncada`, `Poblacion1`]

const [City, Town, Street,] = address;
console.log(`I live ${Street} ${Town} ${City}`)

const animal = {
	animalName: `Lolong`,
	animalWeight: 1075,
	animalFeet: `20`,
	animalInch: `3`
}

console.log(`${animal.animalName} was a saltedwater crocodile. He waighed at ${animal.animalWeight} kgs with a meashurment of ${animal.animalFeet} ft ${animal.animalInch} inc`)

const numbers = [1, 2, 3, 4, 5]

numbers.forEach((number) => {
	console.log(`${number}`)
})


class Dog{
	constructor(name, age, breed) {
		this.name = name,
		this.age = age,
		this.breed = breed
	}
}

const myDog = new Dog()

myDog.name = `Frankie`
myDog.age = 5
myDog.breed = `Miniture Dachshund`
console.log(myDog)